import React from "react";
import Travel from "./Travel.js";

// array

const travelArr = [
  {
    destination: "Skegness",
    quote: "A beautiful day out",
    photo: "https://res.cloudinary.com/edwardwatson/image/upload/c_scale,h_200/v1571344232/Quest%20-%20React%20Travel/blackpool_riifzs.jpg",
  },
  {
    destination: "Southend on Sea",
    quote: "One of the best",
    photo: "https://res.cloudinary.com/edwardwatson/image/upload/c_scale,h_200/v1571344232/Quest%20-%20React%20Travel/southendonsea_rzbjgl.jpg",
  },
  {
    destination: "Bognor Regis",
    quote: "A delight to the senses",
    photo: "https://res.cloudinary.com/edwardwatson/image/upload/c_scale,h_200/v1571344232/Quest%20-%20React%20Travel/bognor_regis_lwgaps.jpg",
  },
  {
    destination: "Skegness",
    quote: "Fish & chips galore",
    photo: "https://res.cloudinary.com/edwardwatson/image/upload/c_scale,h_200/v1571344232/Quest%20-%20React%20Travel/skegness_htwpwm.jpg",
  },
  {
    destination: "Brighton",
    quote: "Everyone loves a pier",
    photo: "https://res.cloudinary.com/edwardwatson/image/upload/c_scale,h_200/v1571344774/Quest%20-%20React%20Travel/brighton_hcjkrf.jpg",
  }
]

const Travels = props => {
  return (
  <figure>
    {travelArr.map((travelobject) => {
      return <Travel travelobject={travelobject}/>
    })

    }
  </figure>)
}

export default Travels;


