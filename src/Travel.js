import React from "react";

const Travel = props => (

    <>
    <h1>{props.travelobject.destination}</h1>
    <h2>{props.travelobject.quote}</h2>
    <img src={props.travelobject.photo} alt=" "></img>
    </>

);

export default Travel;